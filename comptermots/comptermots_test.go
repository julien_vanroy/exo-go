package comptermots

import (
	"testing"
)

func TestCount(t *testing.T) {

	tt := []struct {
		name   string
		chaine string
		nbmot  int
	}{
		{"Chaine vide", "", 0},
		{"Chaine uniquement avec des blancs", "       ", 0},
		{"Chaine avec un mot et blancs avant", "   Test", 1},
		{"Chaine avec un mot et blancs après", "test   ", 1},
		{"Chaine avec un mot et bancs avant et après", "   test  ", 1},
		{"Chaîne avec 4 mots et blancs de taille variables", "   ceci est  un test  ", 4},
		{"Chaîne avec 4 mots et une tabulation", "   ceci est	un test  ", 4},
		{"Chaîne avec 4 mots et un retour à la ligne", "   ceci est\nun test  ", 4},
		{"Chaine normale", "Ceci est un test.", 4},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			s := Count(tc.chaine)
			if s != tc.nbmot {
				t.Errorf("Il n'y a pas %d mots dans la chaine \"%s\"", s, tc.chaine)
			}
		})
	}
}

func BenchmarkCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Count("   ceci est   un test  ")
	}
}
